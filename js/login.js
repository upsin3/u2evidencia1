
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import{ getDatabase,onValue,ref,get,set,child,update,remove }
from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";

import{ getStorage, ref as refS,uploadBytes,getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBqSU5WN2c3ZWLYdPZ_IqUNexNCpYDbFG4",
  authDomain: "sitioweb-f0918.firebaseapp.com",
  databaseURL:"https://sitioweb-f0918-default-rtdb.firebaseio.com",
  storageBucket: "sitioweb-f0918.appspot.com",
  projectId: "sitioweb-f0918",
  storageBucket: "sitioweb-f0918.appspot.com",
  messagingSenderId: "830028204459",
  appId: "1:830028204459:web:c5bb16dc5c43eeb020aaee"
};

// Initialize Firebase

const app = initializeApp(firebaseConfig);
const db=getDatabase();


var login=document.getElementById('login');
var formulario=document.getElementById('formulario');

var usuario;
var contraseña;
var Contraseña;
var btnIniciar=document.getElementById('iniciar');
function ocultar(){
    

    login.style.display="none";
    formulario.style.display="block";

}

function comprobar(){
    leerInputs();
    const dbref=ref(db);
    get(child(dbref,'usuarios/'+usuario)).then((snapshot)=>{
        if(snapshot.exists())
        {
            Contraseña=snapshot.val().Contraseña;
            verificar();
        }
        else{
            alert("Usuario O Contraseña Erroneas");
        }
    }).catch((error)=>{
        alert("Se ha encontrado un Error: "+error);
    });
}

function leerInputs(){
    usuario=document.getElementById('usuario').value;
    contraseña=document.getElementById('contraseña').value;
}


function verificar(){
    if(contraseña==Contraseña)
    {
        alert("Inicio de Sesion Correcto");
        ocultar();
    }
    else{
        alert("Usuario O Contraseña Erroneas");
    }
}

btnIniciar.addEventListener('click',comprobar);